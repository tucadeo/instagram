﻿namespace DesktopUI.Library.Models
{
    public class UserFormValues
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
