﻿using Application.Errors;
using Application.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Followers.Commands.Add
{
    public class AddFollwersCommandHandler : IRequestHandler<AddFollwersCommand>
    {
        private readonly IUserAccessor _userAccessor;
        private readonly IApplicationDbContext _context;

        public AddFollwersCommandHandler(IUserAccessor userAccessor, IApplicationDbContext context)
        {
            _userAccessor = userAccessor;
            _context = context;
        }

        public async Task<Unit> Handle(AddFollwersCommand request, CancellationToken cancellationToken)
        {
            var observer = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == _userAccessor.GetCurrentUsername());

            var target = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == request.Username);

            if (target == null)
            {
                throw new RestException(HttpStatusCode.NotFound, new { User = "Not Found" });
            }

            var following = await _context.Followings.SingleOrDefaultAsync(x =>
                x.ObserverId == observer.Id && x.TargetId == target.Id);

            if (following != null)
            {
                throw new RestException(HttpStatusCode.BadRequest,
                    new { User = "You are already following this user" });
            }

            if (following == null)
            {
                following = new UserFollowing
                {
                    Observer = observer,
                    Target = target
                };
            }

            _context.Followings.Add(following);

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return Unit.Value;

            throw new Exception("Problem saving changes");
        }
    }
}
