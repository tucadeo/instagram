﻿namespace DesktopUI.EventModels
{
    public class MessageEvent
    {
        public string Username { get; set; }
        public string Predicate { get; set; }
    }
}
